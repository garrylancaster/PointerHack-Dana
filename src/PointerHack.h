/* PointerHack.h */
#define CREATOR_ID 'RmG0'
#define RESOURCE_ID 1000 

#define PREF_ID 0
#define PREF_VERSION 1
#define PREF_SAVED true

#define FTR_ENABLED  1
#define FTR_DISABLED 0
#define FTR_NUMBER_ENABLED 1
#define FTR_NUMBER_ENABLECHAR 2

#define POINTER_SIZE 15 //Pixels across and down
#define POINTER_SPEED_FAST 8
#define POINTER_SPEED_SLOW 3

#define MAG_SCALE (Int32)1000

typedef struct
{
    Boolean IsVisible;
    Boolean IsEnabled;
    Int32 DigMaxX;
    Int32 DigMaxY;
    Int32 MagDigX;
    Int32 MagDigY;
    Int32 ScreenMaxX;
    Int32 ScreenMaxY;
    Int32 CurrentX;
    Int32 CurrentY;
    Char EnableChar;
} AppPrefType;

typedef  AppPrefType *AppPrefPtr;

#define PREF_SIZE sizeof(AppPrefType)
