/* PointerHack.c*/

/*
Changes done:
-------------
2001/12/28 - First version
2001/01/02 - Added option 'p' = Move cursor to Insertion Point (i.e. Cursor location)
2001/01/05 - Added configurable launcher key
2001/01/08 - Fixed Menu Control - now don't need to tap '1' twice :)

2021/01/30 (Garry Lancaster):
                Rewrote tune algorithm and fixed to support AlphaSmart Dana
                Added separate defaults for Dana
                Prevent pointer being enabled if screen rotated on Dana
                Run configuration form non-rotated to ensure tune successful
                Added option 'w' to toggle WritePad area on Dana
                Added SPACE as synonym for 's' option
                Removed 'z' option
                Changed '1'-'0' menu pulldown options to exit, as easier to
                navigate via keyboard
                Fix for display of enable char in configuration form
*/


#include "PalmOS.h"
#include "PointerHack.h"

// AlphaSmart-specific
#include <Screen.h>
#include <WritePad.h>

Boolean MySysHandleEventTrap (EventPtr event);
void EnablePointer (AppPrefPtr Prefs);
void ShowPointer(AppPrefPtr Prefs);
void HidePointer(AppPrefPtr Prefs);
void KillPointer(AppPrefPtr Prefs);
void DrawPointer(AppPrefPtr Prefs, Int32 X, Int32 Y);
void MovePointer(AppPrefPtr Prefs, Int32 DeltaX, Int32 DeltaY);
void TapWindow(AppPrefPtr Prefs, Int16 X, Int16 Y);
void ProcessMenu(AppPrefPtr Prefs, Int16 MenuSelected);

extern void initPrefs(AppPrefPtr prefP);
extern void savePrefs(AppPrefPtr prefP);
extern void loadPrefs(AppPrefPtr prefP);

Boolean MySysHandleEventTrap (EventPtr event)
{
   Boolean (*oldtrap)(EventPtr); //procedure pointer to old trap;
   UInt32 temp; //for the feature manager call type checking
   Boolean handled;
   AppPrefType MyPrefs;
   Coord extentX, extentY;
   RectangleType rP;
   EventType MyEvent;
   Int16 tmpX, tmpY;

   UInt32 version;
   UInt32 ftrEnabled;
   UInt32 ftrEnableChar;
   
   FtrGet(CREATOR_ID,RESOURCE_ID,&temp); //get old trap address from HackMaster
   oldtrap=(Boolean (*)(EventPtr))temp; //set procedure pointer

   handled=false;

   //See if enabled
   if (FtrGet(CREATOR_ID, FTR_NUMBER_ENABLED, &ftrEnabled) != 0)
   {
        FtrSet(CREATOR_ID, FTR_NUMBER_ENABLED, FTR_DISABLED);
        MyPrefs.IsEnabled = false;
   }
   else
   {
        if (ftrEnabled == FTR_ENABLED)
        {
            MyPrefs.IsEnabled = true;
        }
        else
        {
            MyPrefs.IsEnabled = false;
        }
   }
   
   //Get Enable Character
   if (FtrGet(CREATOR_ID, FTR_NUMBER_ENABLECHAR, &ftrEnableChar) != 0)
   {
        loadPrefs(&MyPrefs);
        FtrSet(CREATOR_ID, FTR_NUMBER_ENABLECHAR, MyPrefs.EnableChar);
   }
   else
   {
        MyPrefs.EnableChar = ftrEnableChar;
   }
    
   if (MyPrefs.IsEnabled)
   {
       loadPrefs(&MyPrefs);
       switch (event->eType)
       {
        case keyDownEvent :
            handled = true;
            switch (event->data.keyDown.chr)
            {
                    //Normal speed
                    case 'i' : //UP
                    case chrUpArrow : //UP
                        MovePointer(&MyPrefs, 0,-POINTER_SPEED_FAST);
                        break;

                    case 'm' : //DOWN
                    case chrDownArrow : //DOWN
                        MovePointer(&MyPrefs,0,POINTER_SPEED_FAST);
                        break;

                    case 'j' : //LEFT
                    case chrLeftArrow : //LEFT
                        MovePointer(&MyPrefs,-POINTER_SPEED_FAST,0);
                        break;

                    case 'k' : //RIGHT
                    case chrRightArrow : //RIGHT
                        MovePointer(&MyPrefs,POINTER_SPEED_FAST,0);
                        break;

                    case 'o' : //UP & RIGHT
                        MovePointer(&MyPrefs,POINTER_SPEED_FAST,-POINTER_SPEED_FAST);
                        break;

                    case ',' : //DOWN & RIGHT
                        MovePointer(&MyPrefs,POINTER_SPEED_FAST,POINTER_SPEED_FAST);
                        break;

                    case 'u' : //UP & LEFFT
                        MovePointer(&MyPrefs,-POINTER_SPEED_FAST,-POINTER_SPEED_FAST);
                        break;                       

                    case 'n' : //DOWN & LEFT
                        MovePointer(&MyPrefs,-POINTER_SPEED_FAST,POINTER_SPEED_FAST);
                        break;

                    //Fine Tune
                    case 'I' : //UP
                        MovePointer(&MyPrefs, 0,-POINTER_SPEED_SLOW);
                        break;

                    case 'M' : //DOWN
                        MovePointer(&MyPrefs,0,POINTER_SPEED_SLOW);
                        break;

                    case 'J' : //LEFT
                        MovePointer(&MyPrefs,-POINTER_SPEED_SLOW,0);
                        break;

                    case 'K' : //RIGHT
                        MovePointer(&MyPrefs,POINTER_SPEED_SLOW,0);
                        break;

                    case 'O' : //UP & RIGHT
                        MovePointer(&MyPrefs,POINTER_SPEED_SLOW,-POINTER_SPEED_SLOW);
                        break;

                    case '<' : //DOWN & RIGHT
                        MovePointer(&MyPrefs,POINTER_SPEED_SLOW,POINTER_SPEED_SLOW);
                        break;

                    case 'U' : //UP & LEFT
                        MovePointer(&MyPrefs,-POINTER_SPEED_SLOW,-POINTER_SPEED_SLOW);
                        break;

                    case 'N' : //DOWN & LEFT
                        MovePointer(&MyPrefs,-POINTER_SPEED_SLOW,POINTER_SPEED_SLOW);
                        break;

                    case 'p' : //Go to Insertion Point (i.e. Cursor location)
                        InsPtGetLocation(&tmpX, &tmpY);
                        WinGetWindowBounds(&rP);
                        DrawPointer(&MyPrefs, tmpX - rP.topLeft.x, tmpY - rP.topLeft.y);                        
                        break;


                    //Miscellaneous
                    case 't' : //Top
                        DrawPointer(&MyPrefs,MyPrefs.CurrentX, 7);
                        break;

                    case 'b' : //Bottom
                        DrawPointer(&MyPrefs,MyPrefs.CurrentX, MyPrefs.ScreenMaxY - 10);
                        break;

                    case 'l' : //Left
                        DrawPointer(&MyPrefs,10, MyPrefs.CurrentY);
                        break;

                    case 'r' : //Right
                        DrawPointer(&MyPrefs,MyPrefs.ScreenMaxX - 7, MyPrefs.CurrentY);
                        break;

                    case 'c' : //Centre
                        DrawPointer(&MyPrefs,MyPrefs.ScreenMaxX/2 , MyPrefs.ScreenMaxY/2);
                        break;

                    case '1' : //Pull Down Menu 1
                        ProcessMenu(&MyPrefs, 1);
                        break;
                        
                    case '2' : //Pull Down Menu 2
                        ProcessMenu(&MyPrefs, 2);
                        break;
                        
                    case '3' : //Pull Down Menu 3
                        ProcessMenu(&MyPrefs, 3);
                        break;
                        
                    case '4' : //Pull Down Menu 4
                        ProcessMenu(&MyPrefs, 4);
                        break;
                    
                    case '5' : //Pull Down Menu 5
                        ProcessMenu(&MyPrefs, 5);
                        break;
                    
                    case '6' : //Pull Down Menu 6
                        ProcessMenu(&MyPrefs, 6);
                        break;
                    
                    case '7' : //Pull Down Menu 7
                        ProcessMenu(&MyPrefs, 7);
                        break;
                    
                    case '8' : //Pull Down Menu 8
                        ProcessMenu(&MyPrefs, 8);
                        break;
                    
                    case '9' : //Pull Down Menu 9
                        ProcessMenu(&MyPrefs, 9);
                        break;
                    
                    case '0' : //Pull Down Menu 10
                        ProcessMenu(&MyPrefs, 10);
                        break;
                    
                    case 's' : //SELECT single click
                    case chrSpace :
                        TapWindow(&MyPrefs, MyPrefs.CurrentX, MyPrefs.CurrentY);
                        break;

                    case '^' : //SELECT outside window
                        WinGetWindowBounds(&rP);
                        TapWindow(&MyPrefs, -2, rP.extent.y);
                        break;

                    case '.' : //DISABLE
                        HidePointer(&MyPrefs);
                        KillPointer(&MyPrefs);
                        //KillMenu(&MyPrefs);
                        break;

                    case 'w' : // Toggle Dana writepad area
                        if (_WritePadFeaturePresent(&version))
                        {
                                if (WrtpWindowMaximized())
                                {
                                        WrtpMinimizeWindow();
                                }
                                else
                                {
                                        WrtpMaximizeWindow();
                                }
                        }
                        break;


                    default : 
                        break;
                } //switch (event->data.keyDown.chr)
                
                if (event->data.keyDown.chr == MyPrefs.EnableChar) //DO ACTUAL CHARACTER - NOT ENABLE
                {
                        HidePointer(&MyPrefs);
                        KillPointer(&MyPrefs);
                        handled = false;
                }
                break;
       
        case appStopEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            //KillMenu(&MyPrefs);
            handled = false;
            break;

        case tblSelectEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        case ctlSelectEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        case lstSelectEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        case popSelectEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        case daySelectEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        case frmTitleSelectEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        case ctlRepeatEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        case sclRepeatEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        case winExitEvent :
            HidePointer(&MyPrefs);
            KillPointer(&MyPrefs);
            break;

        default:
            break;

       } //switch (event->eType)

    savePrefs(&MyPrefs);
   } //if (MyPrefs.IsEnabled)

   else //Not enabled
   {
        if (event->data.keyDown.chr == MyPrefs.EnableChar && event->eType == keyDownEvent)
        {
                ScrnRotateModeType scrnRotate = rotateScrnMode0;

                if (_ScreenFeaturePresent(&version))
                {
                        ScrnGetRotateMode(&scrnRotate);
                }

                if (scrnRotate == rotateScrnMode0)
                {
                    loadPrefs(&MyPrefs);

                    WinGetWindowExtent(&extentX, &extentY);
                    MyPrefs.ScreenMaxX = extentX;
                    MyPrefs.ScreenMaxY = extentY;
                    MyPrefs.CurrentX = 10;
                    MyPrefs.CurrentY = 7;

                    EnablePointer(&MyPrefs);
                    ShowPointer(&MyPrefs);

                    handled = true;
                    savePrefs(&MyPrefs);
                }
        }
   } //else: if (MyPrefs.IsEnabled)

   if (MyPrefs.IsEnabled)
   {
        FtrSet(CREATOR_ID, FTR_NUMBER_ENABLED, FTR_ENABLED);
   }
   else
   {
        FtrSet(CREATOR_ID, FTR_NUMBER_ENABLED, FTR_DISABLED);
   }
       
   if (!handled) //if you didn't handle it, call the old routine
   { 
       handled=oldtrap(event);
   } //if (!handled)
   return handled;
}
                            
void EnablePointer (AppPrefPtr Prefs)
{
    Prefs->IsEnabled = true;
}

void ShowPointer (AppPrefPtr Prefs)
{
    if (Prefs->IsEnabled)
    {
        DrawPointer(Prefs, Prefs->CurrentX, Prefs->CurrentY);
        Prefs->IsVisible = true;
    }
}

void HidePointer (AppPrefPtr Prefs)
{
    if (Prefs->IsEnabled)
    {
        Prefs->IsVisible = false;
        DrawPointer(Prefs, Prefs->CurrentX, Prefs->CurrentY); //Turn off
    }
}

void KillPointer (AppPrefPtr Prefs)
{
    Prefs->IsEnabled = false;
}

void DrawPointer(AppPrefPtr Prefs, Int32 X, Int32 Y)
{
    if (Prefs->IsVisible)  //Remove old first
    {
        WinInvertLine (Prefs->CurrentX + 1, Prefs->CurrentY, Prefs->CurrentX + POINTER_SIZE - 1, Prefs->CurrentY + 8); 
        WinInvertLine (Prefs->CurrentX, Prefs->CurrentY, Prefs->CurrentX + 8, Prefs->CurrentY + POINTER_SIZE - 1); 
        WinInvertLine (Prefs->CurrentX + POINTER_SIZE, Prefs->CurrentY + 9, Prefs->CurrentX + 9, Prefs->CurrentY + POINTER_SIZE); 
    }

    if (X > Prefs->ScreenMaxX) X = Prefs->ScreenMaxX;
    if (X < 1) X = 1;
    if (Y > Prefs->ScreenMaxY) Y = Prefs->ScreenMaxY;
    if (Y < 1) Y = 1;

    Prefs->CurrentX = X;
    Prefs->CurrentY = Y;
    
    WinInvertLine (Prefs->CurrentX + 1, Prefs->CurrentY, Prefs->CurrentX + POINTER_SIZE - 1, Prefs->CurrentY + 8); 
    WinInvertLine (Prefs->CurrentX, Prefs->CurrentY, Prefs->CurrentX + 8, Prefs->CurrentY + POINTER_SIZE - 1); 
    WinInvertLine (Prefs->CurrentX + POINTER_SIZE, Prefs->CurrentY + 9, Prefs->CurrentX + 9, Prefs->CurrentY + POINTER_SIZE); 
}

void MovePointer(AppPrefPtr Prefs, Int32 DeltaX, Int32 DeltaY)
{
    DrawPointer(Prefs, Prefs->CurrentX + DeltaX, Prefs->CurrentY + DeltaY);
}

void TapWindow(AppPrefPtr Prefs, Int16 X, Int16 Y)
{
        UInt32 version;
        RectangleType rP;
        PointType ptPenPosition;

        HidePointer(Prefs);
        KillPointer(Prefs);

        WinGetWindowBounds(&rP);

        if (_ScreenFeaturePresent(&version))
        {
                ScrnSystemStateType scrnState;
                ScrnGetSystemState(&scrnState);

                if (scrnState.screenMode == screenModeCentered)
                {
                        rP.topLeft.x += scrnState.offset_X;
                        rP.topLeft.y += scrnState.offset_Y;
                }
        }

        //Pen Down
        EvtFlushPenQueue();
        ptPenPosition.x = Prefs->DigMaxX -
               (((rP.topLeft.x + X) * Prefs->MagDigX) / MAG_SCALE);
        ptPenPosition.y = Prefs->DigMaxY -
               (((rP.topLeft.y + Y) * Prefs->MagDigY) / MAG_SCALE);
        EvtEnqueuePenPoint(&ptPenPosition);

        //Pen Up
        ptPenPosition.x = -1;
        ptPenPosition.y = -1;
        EvtEnqueuePenPoint(&ptPenPosition);
}

void ProcessMenu(AppPrefPtr Prefs, Int16 MenuSelected)
{
    MenuBarPtr mbtMenu;
    EventType MyEvent;
    FormPtr ftMenu;

    HidePointer(Prefs);
    KillPointer(Prefs);

    mbtMenu = MenuGetActiveMenu();

    if (mbtMenu == NULL) //If not active, activate it.
    {
        ftMenu = FrmGetActiveForm();
        if (ftMenu->menuRscId != 0)
        {
            mbtMenu = MenuInit(ftMenu->menuRscId);
            MenuSetActiveMenu(mbtMenu);
            mbtMenu = MenuGetActiveMenu();
            if (mbtMenu && MenuSelected > 0 && MenuSelected <= mbtMenu->numMenus)
            { 
                mbtMenu->curMenu = MenuSelected - 1; //zero-based
                mbtMenu->attr.visible = false; //zero-based
                mbtMenu->curItem = noMenuItemSelection;
                
                MyEvent.eType = keyDownEvent;
                MyEvent.data.keyDown.chr = vchrMenu;
                MyEvent.data.keyDown.modifiers = 0;
                EvtAddEventToQueue(&MyEvent);
            }
        }
    }
}
