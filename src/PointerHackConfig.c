#include <PalmOS.h>
#include "PointerHackConfig_rcp.h"
#include "PointerHack.h"

// AlphaSmart-specific
#include <Screen.h>


Boolean ConfigEventHandler(EventPtr eventP);
void Tune(AppPrefPtr prefP);
void DisplayCurrent(FormPtr frmP, AppPrefPtr prefs);
static void SetFieldText(Int16 nFieldName, char * sNewText);
char *GetFieldText(Int16 nFieldName);

extern void initPrefs(AppPrefPtr prefP);
extern void savePrefs(AppPrefPtr prefP);
extern void loadPrefs(AppPrefPtr prefP);

// Program

Boolean ConfigEventHandler(EventPtr eventP)
{
    Boolean handled = false;
    UInt32 version;

    FormPtr frmP = FrmGetActiveForm();
    char *cText;

    AppPrefType prefs;

    loadPrefs(&prefs);

    switch (eventP->eType)
    {
        case frmOpenEvent :
            if (_ScreenFeaturePresent(&version))
            {
                    // Ensure unrotated, otherwise tuning won't work.
                    ScrnSetRotateMode(rotateScrnMode0);
            }
            FrmDrawForm(frmP);
            DisplayCurrent(frmP, &prefs);
            break;

        case frmUpdateEvent :
            handled = true;
            break;

        case ctlSelectEvent :
        {
            switch (eventP->data.ctlSelect.controlID)
            {
                case TUNE_BUTTON :
                    Tune(&prefs);
                    savePrefs(&prefs);
                    //from the HackMaster API
                    FrmGotoForm(2000);
                    handled = true;
                    break;

                case DEFAULT_BUTTON :
                    initPrefs(&prefs);
                    //from the HackMaster API
                    FrmGotoForm(2000);
                    handled = true;
                    break;

                case OK_BUTTON :
                    cText = GetFieldText(FLD_ENABLE_CHAR);
                    if (cText == NULL || StrLen(cText) == 0)
                    {
                       FrmAlert(ALERT_INVALID_CHAR);
                    }
                    else
                    {
                        prefs.EnableChar = cText[0];
                        savePrefs(&prefs);
                        FtrSet(CREATOR_ID, FTR_NUMBER_ENABLECHAR, prefs.EnableChar);
                        //WinDrawChars(&prefs.EnableChar,1,0,120);
                        //from the HackMaster API
                        FrmGotoForm(9000);
                    }

                    handled = true;
                    break;
            }
        }
        default :
            handled = false;
            break;
    }
    return handled;
}

void Tune(AppPrefPtr prefs)
{
        char PosT[13],
              XPosT[6],
              YPosT[6];
        char sTemp[100];

        RectangleType rP, rWin;
        PointType ptPenPosition;
        Boolean IsPenDown = false;
        UInt32 version;
        Int16 ScrX;
        Int16 ScrY;
        Int16 offsetX = 0;
        Int16 offsetY = 0;
        Int32 ValX;
        Int32 ValY;
        Int32 MagX = 0;
        Int32 MagY = 0;

        //Clearing screen
        rP.topLeft.x = 0;
        rP.topLeft.y = 20;
        rP.extent.x = 160;
        rP.extent.y = 40;
        WinEraseRectangle(&rP,0);

        //Display Labels
        StrCopy(sTemp, "Digitizer (Width,Height): ");
        WinDrawChars(sTemp, StrLen(sTemp), 1, 25);
        StrCopy(sTemp, "Magnification (X,Y) : ");
        WinDrawChars(sTemp, StrLen(sTemp), 1, 40);

        // Get window offset.
        if (_ScreenFeaturePresent(&version))
        {
                ScrnSystemStateType scrnState;
                ScrnGetSystemState(&scrnState);

                if (scrnState.screenMode == screenModeCentered)
                {
                        offsetX = scrnState.offset_X;
                        offsetY = scrnState.offset_Y;
                }
        }

        // Get window extent.
        WinGetDrawWindowBounds(&rWin);

        // 1. Find maximum X.
        ValX = 16384;
        ScrX = -offsetX;
        ValY = 100;

        rP.topLeft.x = 110;
        rP.topLeft.y = 25;
        rP.extent.x = 50;
        rP.extent.y = 10;

        while ((ScrX + offsetX) != 1)
        {
                if ((ScrX + offsetX) == 0)
                {
                        ValX -= 200;
                }
                else if ((ScrX + offsetX) > 1)
                {
                        ValX++;
                }
                else
                {
                        ValX--;
                }

                EvtFlushPenQueue();
                ptPenPosition.x = ValX;  
                ptPenPosition.y = ValY;
                EvtEnqueuePenPoint(&ptPenPosition);
                EvtGetPen(&ScrX, &ScrY, &IsPenDown);
                EvtFlushPenQueue();

                WinEraseRectangle(&rP,0);

                StrIToA(XPosT,ValX);
                StrIToA(YPosT,ValY);
                StrCopy(PosT,XPosT);
                StrCat(PosT,",");
                StrCat(PosT,YPosT);
                WinDrawChars(PosT,StrLen(PosT),110,25);
        }

        prefs->DigMaxX = ValX;

        // 2. Find maximum Y.
        ValY = 16384;
        ScrY = -offsetY;

        while ((ScrY + offsetY) != 1)
        {
                if ((ScrY + offsetY) == 0)
                {
                        ValY -= 200;
                }
                else if ((ScrY + offsetY) > 1)
                {
                        ValY++;
                }
                else
                {
                        ValY--;
                }

                EvtFlushPenQueue();
                ptPenPosition.x = ValX;  
                ptPenPosition.y = ValY;
                EvtEnqueuePenPoint(&ptPenPosition);
                EvtGetPen(&ScrX, &ScrY, &IsPenDown);
                EvtFlushPenQueue();

                WinEraseRectangle(&rP,0);

                StrIToA(XPosT,ValX);
                StrIToA(YPosT,ValY);
                StrCopy(PosT,XPosT);
                StrCat(PosT,",");
                StrCat(PosT,YPosT);
                WinDrawChars(PosT,StrLen(PosT),110,25);
        }

        prefs->DigMaxY = ValY;      

        // 3. Find minimum X and therefore magnification.

        rP.topLeft.x = 110;
        rP.topLeft.y = 40;
        rP.extent.x = 50;
        rP.extent.y = 10;

        while (ScrX != (rWin.extent.x - 2))
        {
                if (ScrX < (rWin.extent.x / 2))
                {
                        ValX -= 100;
                }
                else if (ScrX >= (rWin.extent.x - 1))
                {
                        ValX++;
                }
                else
                {
                        ValX--;
                }

                EvtFlushPenQueue();
                ptPenPosition.x = ValX;  
                ptPenPosition.y = ValY;
                EvtEnqueuePenPoint(&ptPenPosition);
                EvtGetPen(&ScrX, &ScrY, &IsPenDown);
                EvtFlushPenQueue();

                WinEraseRectangle(&rP,0);

                MagX = (prefs->DigMaxX - ValX) * MAG_SCALE / (offsetX + rWin.extent.x - 2);

                StrIToA(XPosT,MagX);
                StrIToA(YPosT,MagY);
                StrCopy(PosT,XPosT);
                StrCat(PosT,",");
                StrCat(PosT,YPosT);
                WinDrawChars(PosT,StrLen(PosT),110,40);
        }

        prefs->MagDigX = MagX;

        // 4. Find minimum Y and therefore magnification.

        while (ScrY != (rWin.extent.y - 2))
        {
                if (ScrY < (rWin.extent.y / 2))
                {
                        ValY -= 100;
                }
                else if (ScrY >= (rWin.extent.y - 1))
                {
                        ValY++;
                }
                else
                {
                        ValY--;
                }

                EvtFlushPenQueue();
                ptPenPosition.x = ValX;  
                ptPenPosition.y = ValY;
                EvtEnqueuePenPoint(&ptPenPosition);
                EvtGetPen(&ScrX, &ScrY, &IsPenDown);
                EvtFlushPenQueue();

                WinEraseRectangle(&rP,0);

                MagY = (prefs->DigMaxY - ValY) * MAG_SCALE / (offsetY + rWin.extent.y - 2);

                StrIToA(XPosT,MagX);
                StrIToA(YPosT,MagY);
                StrCopy(PosT,XPosT);
                StrCat(PosT,",");
                StrCat(PosT,YPosT);
                WinDrawChars(PosT,StrLen(PosT),110,40);
        }

        prefs->MagDigY = MagY;
}

void DisplayCurrent(FormPtr frmP, AppPrefPtr prefs)
{
        char PosT[20],
              XPosT[10],
              YPosT[10];
        char sTemp[100];

        RectangleType rP;
        Int32 ValX;
        Int32 ValY;

        //Clearing screen
        rP.topLeft.x = 0;
        rP.topLeft.y = 20;
        rP.extent.x = 160;
        rP.extent.y = 40;
        WinEraseRectangle(&rP,0);

        //Display Labels
        StrCopy(sTemp, "Digitizer (Width,Height): ");
        WinDrawChars(sTemp, StrLen(sTemp), 1, 25); 
        StrCopy(sTemp, "Magnification (X,Y) : ");
        WinDrawChars(sTemp, StrLen(sTemp), 1, 40);  

        ValX = prefs->DigMaxX;
        ValY = prefs->DigMaxY;
        StrIToA(XPosT,ValX);
        StrIToA(YPosT,ValY);
        StrCopy(PosT,XPosT);
        StrCat(PosT,",");
        StrCat(PosT,YPosT);
        WinDrawChars(PosT,StrLen(PosT),110,25);

        ValX = prefs->MagDigX;
        ValY = prefs->MagDigY;
        StrIToA(XPosT,ValX);
        StrIToA(YPosT,ValY);
        StrCopy(PosT,XPosT);
        StrCat(PosT,",");
        StrCat(PosT,YPosT);
        WinDrawChars(PosT,StrLen(PosT),110,40);

        sTemp[0] = prefs->EnableChar;
        sTemp[1] = '\0';
        SetFieldText(FLD_ENABLE_CHAR, sTemp);
}

static void SetFieldText(Int16 nFieldName, char * sNewText)
{
    FormPtr frmP = FrmGetActiveForm();
    FieldPtr fldP = FrmGetObjectPtr(frmP, FrmGetObjectIndex(frmP, nFieldName));
    MemHandle oldH = FldGetTextHandle(fldP);
    int length = StrLen(sNewText) + 8;
    MemHandle h = MemHandleNew(length + 1);
    StrCopy(MemHandleLock(h), sNewText);
    MemHandleUnlock(h);
    //FldSetText(fldP, h, 0, length + 1);
    FldSetTextHandle(fldP, h);
    FldDrawField(fldP);
    if (oldH != NULL) 
        MemHandleFree(oldH);
} 

char *GetFieldText(Int16 nFieldName)
{
    char * sFieldText;
    FormPtr frmP = FrmGetActiveForm();
    FieldPtr fldP = FrmGetObjectPtr(frmP, FrmGetObjectIndex(frmP, nFieldName));
    MemHandle h = FldGetTextHandle(fldP);
    if (h != NULL) 
    {
        sFieldText = MemHandleLock(h);
        MemHandleUnlock(h);
    }
    else
    {
        sFieldText = NULL;
    }
    return sFieldText;
}
