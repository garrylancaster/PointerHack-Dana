#include <PalmOS.h>
#include "PointerHack.h"

// AlphaSmart-specific
#include <Screen.h>


void savePrefs(AppPrefPtr prefP)
{
    PrefSetAppPreferences(CREATOR_ID, PREF_ID, PREF_VERSION, prefP, PREF_SIZE, PREF_SAVED);
}

void initPrefs(AppPrefPtr prefP)
{
        UInt32 width, height, depth;
        Boolean colourUsable;
        WinScreenMode(winScreenModeGet, &width, &height, &depth, &colourUsable);

        if (width >= 560)
        {
                // Dana defaults.
                prefP->DigMaxX = 3930;
                prefP->DigMaxY = 230;
                prefP->MagDigX = 6780;
                prefP->MagDigY = 1300;
        }
        else
        {
                // PDA defaults.
                prefP->DigMaxX = 230;
                prefP->DigMaxY = 230;
                prefP->MagDigX = 1250;
                prefP->MagDigY = 950;
        }

        prefP->EnableChar = '\\';
        prefP->IsVisible = false;

        savePrefs(prefP);
}

void loadPrefs(AppPrefPtr prefP)
{
    Int32 found;
    UInt16 prefsSize = PREF_SIZE;

    found = PrefGetAppPreferences(CREATOR_ID, PREF_ID, prefP, &prefsSize, PREF_SAVED);

    if ((found == noPreferenceFound) || (prefsSize != PREF_SIZE))
    {
        initPrefs(prefP);
    }
}
